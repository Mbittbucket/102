package PracaDomowa04;

public class DrzewoLisciaste extends Drzewo {
    private int ksztaltLiscia;

    public DrzewoLisciaste() {
    }

    public DrzewoLisciaste(int ksztaltLiscia) {
        this.ksztaltLiscia = ksztaltLiscia;
    }

    @Override
    public String toString() {
        return "DrzewoLisciaste{" +
                "ksztaltLiscia=" + ksztaltLiscia +
                '}' + super.toString();
    }
}
