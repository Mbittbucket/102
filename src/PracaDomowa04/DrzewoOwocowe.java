package PracaDomowa04;

public class DrzewoOwocowe extends DrzewoLisciaste {

    private String nazwaOwoca;

    public DrzewoOwocowe(String nazwaOwoca) {
        this.nazwaOwoca = nazwaOwoca;
    }

    @Override
    public String toString() {
        return "DrzewoOwocowe{" +
                "nazwaOwoca='" + nazwaOwoca + '\'' +
                '}' + super.toString();
    }

}
