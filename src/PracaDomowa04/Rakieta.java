package PracaDomowa04;

public class Rakieta {
    private String name ;
    private int fuel ;

    public Rakieta(String name, int fuel) {
        this.name = name;
        this.fuel = fuel;
        if (this.fuel < 0){
            throw new IllegalArgumentException("Nie możesz mieć ujemnej wartości paliwa");
        }
    }

    public void reFuel (){
        this.fuel += (int) Math.random() * 1000;
    }

    public void start (){
        if ( this.fuel < 1000) {
            System.out.println("Za mało paliwa, prosze zatankować ponownie");
        }
        else System.out.println("Start!!");
    }

    public static void main(String[] args) {
       try {Rakieta rakieta = new Rakieta("Andrzej", 1000);
       rakieta.reFuel();
       rakieta.start();
       }
       catch (IllegalArgumentException e) {
           e.printStackTrace();
       }



    }

}
