package PracaDomowa04;

public class DrzewoIglaste extends Drzewo {

    private int iloscIgiel;
    private double dlugoscSzyszki;

    public DrzewoIglaste() {
    }

    public DrzewoIglaste(int iloscIgiel, double dlugoscSzyszki) {
        this.iloscIgiel = iloscIgiel;
        this.dlugoscSzyszki = dlugoscSzyszki;
    }

    @Override
    public String toString() {
        return "DrzewoIglaste{" +
                "iloscIgiel=" + iloscIgiel +
                ", dlugoscSzyszki=" + dlugoscSzyszki +
                '}' + super.toString();
    }
}
