package Week4;

import java.io.IOException;
import java.nio.file.Files;

import java.nio.file.Paths;

public class Hello5 {

    public static void main(String[] args) {
        try {
            Files.createFile(Paths.get("hello5.txt"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            Files.move(Paths.get("hello5.txt"), Paths.get("hello5_1.txt"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            Files.copy(Paths.get("hello5_1.txt"),Paths.get("hello5_2.txt"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            Files.delete(Paths.get("hello5_1"));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
