package Week4;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Pracownik {
    private int id;
    private int wiek;



    public Pracownik(int id, int wiek) {
        this.id = id;
        this.wiek = wiek;
    }

    public static void main(String[] args) {

      /*  Pracownik p = new Pracownik(12,23);

        try {
            ObjectOutputStream out = new ObjectOutputStream(Files.newOutputStream(Paths.get("Pracownik1.txt")));
            out.writeObject(p);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
*/

        try {
            ObjectInputStream in = new ObjectInputStream(Files.newInputStream(Paths.get("Pracownik1.txt")));
            try {
                Pracownik pracownik = (Pracownik)in.readObject();
                System.out.println(pracownik);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }


}
