package Week4;

import java.awt.geom.Point2D;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Figura {

    private String opis;
    private Point2D point2D;

    public static void main(String[] args) {
        Figura f = new Figura();
        Point2D point2D = new Point2D.Double(12,43);

        try (ObjectOutputStream out = new ObjectOutputStream(Files.newOutputStream(Paths.get("Figura.txt")))){
            out.writeObject(f);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}
