package Week4;

import java.io.UnsupportedEncodingException;

public class Szyfr {
    public static void main(String[] args) {

        int[] tab = {68, 112, 100, 117, 125, 37, 118, 118, 107, 131, -50, -112, 128, 114, 121, 47, 132, 128, 50, 131,
                131, -38, -104, 134, 143, 122, 58, 141, 139, 127, 141, 147, 153, 79};


        int[] newTab = new int[tab.length];
        for (int i = 0; i < tab.length; i++){
            newTab[i] = tab[i] - i;
        }
        
        byte[] b = new byte[newTab.length];
        for (int i = 0; i <newTab.length; i++){
            b[i] = (byte)newTab[i];
        }
        String s = null;

        try {
            s = new String(b, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        System.out.println(s);
    }
}
