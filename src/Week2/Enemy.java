package Week2;

public class Enemy {
    private double hP;
    private double aP;
    private  double dP;

    public Enemy() {
    }

    public Enemy(double hP, double aP, double dP) {
        this.hP = hP;
        this.aP = aP;
        this.dP = dP;
    }

    public void sethP(double hP) {
        this.hP = hP;
    }

    public void setaP(double aP) {
        this.aP = aP;
    }

    public void setdP(double dP) {
        this.dP = dP;
    }

    public String attack() {
        String a = ": zadałem " + aP + " punktów obrażeń";
        return a;
    }
    public String defend() {
        String d = ": pochłonąłem " + dP + " punkót obrażeń";
        return  d;
    }
}
