package Week2;

public class MovablePoint implements  Movable {
    protected  int x;
    protected  int y;
    protected int xSpeed;
    protected int ySpeed;

    @Override
    public String toString() {
        return "MovablePoint{" +
                "x=" + x +
                ", y=" + y +
                ", xSpeed=" + xSpeed +
                ", ySpeed=" + ySpeed +
                '}';
    }

    public MovablePoint() {
    }

    public MovablePoint(int x, int y, int xSpeed, int ySpeed) {
        this.x = x;
        this.y = y;
        this.xSpeed = xSpeed;
        this.ySpeed = ySpeed;

    }


    @Override
    public void moveU() {

    }

    @Override
    public void moveD() {

    }

    @Override
    public void moveL() {

    }

    @Override
    public void moveR() {

    }
}
