package Week2;

    public class MyArrayList<T> {

        private int size;

        private Object[] elements;
        public MyArrayList() {

            elements = new Object[10];
            size = 0 ;

        }
        public void add(T o) {

            if (size < elements.length) {
                elements[size] = o;
                ++size;

            }

        }
        public T get(int index) {

            if (index >= size)
                throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);
            return (T) elements[index];

        }
        public int size() { return size; }

    }
