package Week2;
public class Triangle implements Shape {


    private double a, h;
    private String color;

    public Triangle(double a, double h, String color) {

        this.a = a;
        this.h = h;
        this.color = color;
    }



    @Override

    public double calcArea() {

        return 0.5*a*h;

    }

    @Override

    public String toString() {

        return "Jestem trójkątem o wysokości " + h + " i podstawie " + a + ", koloru "
                + color +   //pole protected z klasy rodzica, dostep bezposredni
                " - moje pole powierzchni wynosi " + calcArea();

    }

}