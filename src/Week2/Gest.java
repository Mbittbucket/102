package Week2;

import javafx.print.Paper;

public enum Gest {

 ROCK{
     @Override
     GameState play(Gest g) {
          switch (g){
             case ROCK:
                 return GameState.DRAW;

             case PAPER:
                 return  GameState.LOSE;

             default:
                 return GameState.WIN;

         }

     }
 },
    PAPER {
        @Override
        GameState play(Gest g) {
            switch (g) {
                case SCISSORS:
                    return GameState.LOSE;
                case PAPER:
                    return GameState.DRAW;
                default:
                    return GameState.WIN;


            }
        }
    } ,
    SCISSORS{
        @Override
        GameState play(Gest g) {
            switch (g){
                case ROCK:
                    return GameState.LOSE;

                case PAPER:
                    return GameState.WIN;

                    default:
                    return  GameState.DRAW;

            }
        }
    } ;

 abstract GameState play(Gest g) ;


}
