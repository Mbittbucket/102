package Week2;


public class Point {

    private int x;
    private int y;

    public Point() {
        x = 0;
        y = 0;

    }
    public Point(int x, int y) {

        this.x = x;
        this.y = y;
    }

    public double distance(int x, int y) {

        return Math.sqrt(Math.pow(this.x - x, 2) + Math.pow(this.y - y, 2));
    }

    public double distance() {

        return distance(0, 0);  //uzywamy wczesniej zdefiniowanej funkcji

    }

    public double distance(Point p) {

        return distance(p.getX(), p.getY());

    }

    public static void printDistance(Point p1, Point p2) {

        System.out.println("Odległość punktu " + p1 + " od punktu " + p2 + " wynosi " + p1.distance(p2));

    }

    public void printDistance(Point p1) {
        System.out.println("Odległość punktu " + this + " od punktu " + p1 + " wynosi " + this.distance(p1));
    }
    @Override
    public String toString() {  //chce miec "(x, y)"

        return "(" +  "x=" + x +  ", y=" + y + ')';

    }



    public void setXY(int x, int y) {

        setX(x);    //tak lepiej, bo moze byc sytuacja jak nizej w zakomentowanym fragmencie funkcji setX

        setY(y);

    }


    public int[] getXY() {

        int[] tab = {x, y}; //najkrotszy zapis

        return tab;


    }

    public int getX() {

        return x;
    }

    public void setX(int x) {

        this.x = x;
}

    public int getY() {
        return y;

    }
    public void setY(int y) {

        this.y = y;

    }

}