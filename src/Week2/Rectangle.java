package Week2;

public class Rectangle implements Shape {



    private double a, b;
    private String color;


    public Rectangle(double a, double b, String color) {

        this.a = a;
        this.b = b;
        this.color = color;
    }


    @Override

    public double calcArea() {

        return a * b;

    }

    @Override

    public String toString() {

        return "Jestem prostokątem o wymiarach " + a + "x" + b + ", koloru "
                + color +   //pole protected z klasy rodzica, dostep bezposredni
                " - moje pole powierzchni wynosi " + calcArea();

    }

}