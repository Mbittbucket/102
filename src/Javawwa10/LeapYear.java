package Javawwa10;

public class LeapYear {

    public static void main(String[] args) {    //generate with psvm + enter

        //System.out.println(); // generate with sout + enter

        int testYear = 2016;    //2016 - rok przestepny, 2001 - rok NIE przestepny

        //sprawdzam warunki czy rok jest przestepny
        boolean conditionOne = (testYear % 4 == 0) && (testYear % 100 != 0);
        boolean conditionTwo = testYear % 400 == 0;
        //wyswietlam informacje nt roku przestepnego
        if(conditionOne || conditionTwo) {
            //do this if condition == true
            System.out.println("Rok " + testYear + " jest rokiem przestepnym.");
        }
        else {
            //do this otherwise
            System.out.println("Rok " + testYear + " NIE jest rokiem przestepnym.");
        }
    }
}