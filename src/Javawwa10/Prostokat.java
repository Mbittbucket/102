package Javawwa10;
//1) Utworzyc klase Prostokat
//2) Ustawic domyslne wartosci pol tej klasy (jedna z dwoch poznanych metod)
//3) Zaimplementowac metody obliczajace: calcArea, calcPerimiter
//4) Utworzyc klase Runner z metoda main
//5) W metodzie main klasy Runner utworzyc 3 zadane obiekty klasy Rectangle
//6) Napisac w klasie Runner dodatkowa metode statyczna, ktora dla przekazanego obiektu klasy Prostokat
// wypisuje o nim informacje: "Prostokąt o wymiarach sideA x sideB, koloru color. Jego obwod to ... a pole ... "
//7) Wywolac metode z pkt 6 na obiektach z pkt 5 - w metodzie main klasy Runner
public class Prostokat {
    double bokA = 10.0 ;
    double bokB = 10.0 ;
    String kolor = "czerwony" ;

    public Prostokat(double bokA, double bokB, String kolor) {
        this.bokA = bokA;
        this.bokB = bokB;
        this.kolor = kolor;
    }
    public double calcArea(){
        double calc = bokA * bokB;
        return calc;
    }
    public double calPerimiter(){
        double calc2 = 2 * bokB + 2 * bokA;
        return calc2;
    }
}

