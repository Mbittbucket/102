package Javawwa10;

public class Quadratic {
    public static void main(String[] args) {

        double b = Double.parseDouble(args[0]);
        double c = Double.parseDouble(args[1]);
        double d = b * b - 4.0 * c;


        boolean conditionOne = d == 0  ;
        boolean conditionTwo = d < 0 ;
        boolean conditionThree = d > 0 ;


        if(conditionThree) {

            System.out.println("b = " + b);
            System.out.println("c = " + c);
            //d = b*b - 4.0*c
            System.out.println("delta = " + d);
            //x1 = (-b + sqrt(d)) / 2.0 - samo sqrt nie zadziała, ponieważ potrzebna jest do tego metoda ( Math.sqrt() )
            //oraz
            //x2 = (-b - sqrt(d)) / 2.0
            double x1 = (-b + Math.sqrt(d)) / 2.0;
            double x2 = (-b - Math.sqrt(d)) / 2.0;
        }

        if (conditionOne){
            double x1 = -b / 2.0 ;
            System.out.println(x1);
        }

        if (conditionTwo) {
            System.out.println("Brak rozwiazania w dziedzinie liczb rzeczywistych");
        }





    }
}
