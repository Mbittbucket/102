package Week3;

import java.util.ArrayList;
import java.util.List;

public class Subject  {

    final private String name;

    public Subject(String name) {
        this.name = name;
    }

    public void addStudent(Student s){ student.add(s);}
    public void addLecturer (Lecturer l) { lecturer.add(l);}

    @Override
    public String toString() {
        return "Subject{" +
                "name='" + name + '\'' +
                ", lecturer=" + lecturer +
                ", student=" + student +
                '}';
    }

    List<Lecturer> lecturer = new ArrayList<Lecturer>();
    List<Student>  student = new ArrayList<Student>();

    public List<Lecturer> getLecturer() {
        return lecturer;
    }

    public List<Student> getStudent() {
        return student;
    }
}
