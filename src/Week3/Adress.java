package Week3;

public class Adress {
    private String ulica;
    private  int NumerDomu;
    private int NumerMieszkania;
    private String KodPocztowy;
    private String miasto;

    public Adress() {
    }

    public Adress(String ulica, int numerDomu, int numerMieszkania) {
        this.ulica = ulica;
        this.NumerDomu = numerDomu;
        this.NumerMieszkania = numerMieszkania;
    }

    public Adress(String ulica, int numerDomu, int numerMieszkania, String kodPocztowy, String miasto) {
        this.ulica = ulica;
        this.NumerDomu = numerDomu;
        this.NumerMieszkania = numerMieszkania;
        this.KodPocztowy = kodPocztowy;
        this.miasto = miasto;
    }

    @Override
    public String toString() {
        return "Adress{" +
                "ulica='" + ulica + '\'' +
                ", NumerDomu=" + NumerDomu +
                ", NumerMieszkania=" + NumerMieszkania +
                ", KodPocztowy='" + KodPocztowy + '\'' +
                ", miasto='" + miasto + '\'' +
                '}';
    }


    public String ulica() {
        return "Adress{" +
                "ulica='" + ulica + '\'' +
                ", NumerDomu=" + NumerDomu +
                ", NumerMieszkania=" + NumerMieszkania +
                '}';
    }
}
