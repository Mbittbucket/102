package Week3;

import java.sql.SQLOutput;

/*
Zad 4: Utwórz metodę, która przyjmuje dwa parametry - liczbę ( int ) oraz napis

( String ). Metoda ma wyświetlić podany napis, przekazaną liczbę razy. W

przypadku gdy liczba wystąpień będzie mniejsza bądź równa zero pownien

zostać rzucony wyjątek IndexOutOfBoundsException , a w przypadku gdy

napis będzie null -em pownien zostać rzucony wyjątek

IllegalArgumentException
 */
public class Zad4 {

    private int x;
    private String y;

    public void lol(int x, String y) {
        if (x <= 0) {
            throw new IndexOutOfBoundsException();
        }
        if (y == null) {
            throw new IllegalArgumentException();
        }
        for (int i = 0; i < x; i++) {
            System.out.println(y);
        }

    }
}