package Week3;

public class Student extends Person {

    final private int albumNr;

    public Student(String name, int age, int albumNr) {
        super(name, age);
        this.albumNr = albumNr;
    }

    @Override
    public void printIntroduca() {
        System.out.println(toString());
    }

    @Override
    public String toString() {
        return "Student{" +
                "albumNr=" + albumNr +
                '}' + super.toString() ;
    }
}
