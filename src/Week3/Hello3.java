package Week3;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Hello3 {

    public static void main(String[] args) {
        try {
            OutputStream outputStream = Files.newOutputStream(Paths.get("hello3"));
            DataOutputStream dataOutputStream = new DataOutputStream(outputStream);
            {
                dataOutputStream.writeInt(15);
                dataOutputStream.writeInt(11);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            InputStream inputStream = Files.newInputStream(Paths.get("hello3"));
            DataInputStream dataInputStream = new DataInputStream(inputStream);
            {
                System.out.println(dataInputStream.readInt());
                System.out.println(dataInputStream.readInt());
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            OutputStream outputStream = Files.newOutputStream(Paths.get("hello4"));
            DataOutputStream dataOutputStream = new DataOutputStream(outputStream);
            {
                dataOutputStream.writeBytes("Hello");

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (RandomAccessFile file = new RandomAccessFile("hello3", "rw")) {
            file.seek(4);
            file.writeInt(19);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }}