package Week3;

import java.util.ArrayList;
import java.util.List;

public class Academy {

    final private String name;
    final private List<Subject> subject = new ArrayList<Subject>();

    public Academy(String name) {
        this.name = name;
    }

    public void addSubject(Subject x) { subject.add(x);}


    @Override
    public String toString() {
        return "Academy{" +
                "name='" + name + '\'' +
                ", subject=" + subject +
                '}' + super.toString();
    }
}
