package Week3;

public class DemoAcademy {
    public static void main(String[] args) {
        Academy academy = new Academy("UW");
        Subject subject = new Subject("Archeologia");
        subject.addStudent(new Student("Jan Kowalski", 20,21111));
        subject.addStudent(new Student("Roch Jankowski",24,12222));
        subject.addLecturer(new Lecturer("Wojciech P", 26));

        academy.addSubject(subject);
        System.out.println(academy);


    }
}