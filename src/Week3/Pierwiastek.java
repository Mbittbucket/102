package Week3;
/*Zad 1: Utwórz metodę która przyjmuje jeden parametr (typu int ) a następnie

liczy pierwiastek z podanej licby (użyj Math.sqrt() ). Jeśli podany parmetr

będzie liczbą ujemną rzuć wyjątek (np. IllegalArgumentException ) z

komunikatem błędu.*/

import java.util.Scanner;

public class Pierwiastek {
Scanner scanner = new Scanner(System.in);
int c = scanner.nextInt();


    private int x;

    public void setX() {
        this.x = c;
        if ( x < 0){
            throw new IllegalArgumentException ("nie ma takiej możliwości");
        }
    }

    public double liczeP (){
        double pierwiastek = Math.sqrt(x);
        return pierwiastek;
    }

}
