package Week3;

public class Lecturer extends Person {

    public Lecturer(String name, int age) {
        super(name, age);
    }

    @Override
    public String toString() {
        return "Lecturer{}" + super.toString();
    }

    @Override
    public void printIntroduca() {

    }


}
