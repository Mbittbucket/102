package Week3;

public class Pracownik {
    private String name;
    private String surName;
    private String secondName;
    private int age;
    private boolean sex;
    private double salary;
    private Adress adres;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getSecondName() {
        return secondName;
    }
    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }
    public String getSurName() {
        return surName;
    }
    public void setSurName(String surName) {
        this.surName = surName;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public boolean isSex() {
        return sex;
    }
    public void setSex(boolean sex) {
        this.sex = sex;
    }
    public double getSalary() {
        return salary;
    }
        public void setSalary(double salary) {
        this.salary = salary;
    }

    public Pracownik() {
    }

    public Pracownik(String name, String surName) {
        this.name = name;
        this.surName = surName;
    }

    @Override
    public String toString() {
        return "Pracownik{" +
                "name='" + name + '\'' +
                ", surName='" + surName + '\'' +
                ", secondName='" + secondName + '\'' +
                '}';
    }

    public void returnSex (){
        if (sex == true){
            System.out.println("Kobieta");
        }
        else if (sex == false){
            System.out.println("Mezczyzna");
        }
    }

    //metoda, która za pomocą wzoru x = 60 - age lub x1 = 65 - age,
    // określa ile lat pozostało pracownikowi do emerytury.
    // Należy uwzględnić ( ifami na pewno się da) przypadek, w którym daany Pracownik
    // osiągnął już swój wiek emerytalny ( x <= 0)1
}
